from flask import render_template, request
from ies_aps.app import app
import pandas as pd
import sqlite3

@app.route('/index')
@app.route('/')
def index():
    return render_template('index.html')

@app.route("/dadoshh")
def dadoshh():
    conn = sqlite3.connect('investigacao_incidentes.db')
    cursor = conn.cursor()

    #select provisório

    cursor.execute("""
    select (select obra_codigo || ' - ' || obra_nome from obra) as Obra,
       (select empresa_nome from empresa) as Empresa,
       (select colaborador_nome from colaborador) as 'Nome do Envolvido',
       (select tempo_funcao from colaborador) as 'Tempo na Função',
       (select incidente_tipo from incidente) as 'Tipo do Incidente',
       (select cat from incidente) as 'CAT',
       (select data from incidente) as 'Data',
       (select atividade from incidente) as 'Atividade',
       (select afastamento from incidente) as 'Afastamento',
       (select tempo_dias_afastamento from incidente) as 'Dias de Afastamento',
       (select potencial_dano from incidente) as 'Potencial de Dano',
       (select resumo_ocorrencia from incidente) as 'Resumo das ocorrências',
       (select codigo_causas_imediatas || '. ' || causas_imediatas from tasc_causas_imediatas) as 'Causas Imediatas',
       (select codigo_causas_basicas_principais || '. ' || causas_basicas_principais from tasc_causas_basicas) as 'Causas Básicas',
       (select codigo_causas_basicas_secundarias || '. ' || causas_basicas_secundarias from tasc_causas_basicas) as 'Causas Básicas Secundárias',
       (select plano_acao from incidente) as 'Plano de Ação';
    """)

    rows = cursor.fetchall();

    return render_template('dadoshh.html', rows=rows)

@app.route("/estatisticas")
def estatisticas():
    conn = sqlite3.connect('investigacao_incidentes.db')
    cursor = conn.cursor()

    pd.set_option('display.max_rows', None)
    pd.set_option('display.expand_frame_repr', False)
    pd.set_option('max_colwidth', None)

    df = pd.read_sql('''
    select id,
    classificacao,
    codigo_causas_basicas_principais || " - " || causas_basicas_principais as "causas principais",
    codigo_causas_basicas_secundarias || " - " || causas_basicas_secundarias as "causas secundarias"
    from tasc_causas_basicas
    ''', conn)

    cursor.fetchall();

    return render_template('estatisticas.html', df=df)

'''@app.route("/pacientes")
def pacientes():
    Conexao.cur.execute("""select * from hospital.pessoa order by gravidade_paciente desc;""")
    x = str(Conexao.cur.fetchone())
    y = x.replace(',',' || ')
    y = y.replace('(', '')
    y = y.replace(')', '')
    y = y.replace("'", '')
    y = y.replace('datetime', '')
    y = y.replace('.date', '')
    dict = {'x':y}
    return render_template('pacientes.html', dict=dict)

@app.route("/medicos")
def medicos():
    Conexao.cur.execute("""select * from hospital.medicos;""")
    x = str(Conexao.cur.fetchone())
    y = x.replace(',', ' || ')
    y = y.replace('(', '')
    y = y.replace(')', '')
    y = y.replace("'", '')
    dict = {'x': y}
    return render_template('medicos.html', dict=dict)'''
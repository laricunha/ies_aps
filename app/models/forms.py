from wtforms import StringField, DateField, IntegerField, PasswordField, FloatField, BooleanField
from wtforms.form import Form
from wtforms.validators import DataRequired


class CadastroMedico(Form):
    nome_medico = StringField('nome_medico', validators=[DataRequired()])
    sobrenome_medico = StringField('sobrenome_medico', validators=[DataRequired()])
    data_nasc = DateField('data_nasc', validators=[DataRequired()])
    telefone = IntegerField('telefone')
    rg = IntegerField('rg', validators=[DataRequired()])
    cpf = IntegerField('cpf', validators=[DataRequired()])
    genero = StringField('genero', validators=[DataRequired()])
    endereco = StringField('endereco', validators=[DataRequired()])
    crm_medico = IntegerField('crm_medico', validators=[DataRequired()])
    area_atuacao = StringField('area_atuacao', validators=[DataRequired()])
    senha = PasswordField('senha', validators=[DataRequired()])
    confirma_senha = PasswordField('confirma_senha', validators=[DataRequired()])
    email = StringField('email')


class CadastroPaciente(Form):
    nome_paciente = StringField('nome_paciente', validators=[DataRequired()])
    sobrenome_paciente = StringField('sobrenome_paciente', validators=[DataRequired()])
    data_nasc = DateField('data_nasc', validators=[DataRequired()])
    telefone = IntegerField('telefone')
    rg = IntegerField('rg', validators=[DataRequired()])
    cpf = IntegerField('cpf', validators=[DataRequired()])
    genero = StringField('genero', validators=[DataRequired()])
    endereco = StringField('endereco', validators=[DataRequired()])
    email = StringField('email')
    num_emergencia = IntegerField('num_emergencia', validators=[DataRequired()])
    diagnostico = StringField('diagnostico', validators=[DataRequired()])
    quarto = IntegerField('quarto', validators=[DataRequired()])
    andar = IntegerField('andar', validators=[DataRequired()])

class Visita(Form):
    data_visita = DateField('data_visita', validators=[DataRequired()])
    situacao_paciente = StringField('situacao_paciente', validators=[DataRequired()])
    pressao = FloatField('pressao', validators=[DataRequired()])
    temperatura = FloatField('temperatura', validators=[DataRequired()])
    nome_medico = StringField('nome_medico', validators=[DataRequired()])
    crm_medico = IntegerField('crm_medico', validators=[DataRequired()])
    gravidade_paciente = IntegerField('gravidade_paciente', validators=[DataRequired()])
